Modify Docker Root Dir
1. Stop the docker daemon
```shell
sudo service docker stop
```
2. Add daemon.json in /etc/docker
```shell
{ 
   "data-root": "/new_path/docker" 
}
```
3. Copy docker directory to new_path
```shell
sudo rsync -aP /var/lib/docker/ /new_path/docker
```
4. Start the docker daemon
```shell
sudo service docker start
```
5. Remove old docker directory
```shell
sudo rm -rf /var/lib/docker
```
